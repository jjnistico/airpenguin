#ifndef SETTINGS_H
#define SETTINGS_H

#include <QtCore/QObject>

class Vlc : public QObject
{
    Q_GADGET

public:

    /* Playback states */
    enum State
    {
        Idle, Opening, Buffering, Playing, Paused, Stopped, Ended, Error
    };

    Q_ENUM(State);
};

#endif // SETTINGS_H
