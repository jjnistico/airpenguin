#include "vlc/vlc.h"
#include "vlc/audio.h"
#include "vlc/mediaplayer.h"

VlcAudio::VlcAudio(VlcMediaPlayer *player) : QObject(player),
                   _libvlcMediaPlayer(player->libMedia_p())
{}

VlcAudio::~VlcAudio() {}

/* Get current volume
 * Returns volume -> int
 */
int VlcAudio::getVolume() const
{
    int volume = libvlc_audio_get_volume(_libvlcMediaPlayer);

    return volume;
}

/* Get mute state
 * Returns mute state -> bool
 */
bool VlcAudio::getMute() const
{
    bool mute = libvlc_audio_get_mute(_libvlcMediaPlayer);

    return mute;
}

/* Set audio volume
 * @param   volume  int -> Volume to set */
void VlcAudio::setVolume(int volume)
{
    libvlc_audio_set_volume(_libvlcMediaPlayer, volume);

    emit volumeChangedInt(volume);

}

/* Toggle mute of player
 * Some media might not support this */
void VlcAudio::toggleMute() const
{
    libvlc_audio_toggle_mute(_libvlcMediaPlayer);
}
