#ifndef ARGS_H
#define ARGS_H

#include <QString>
#include <QStringList>

#define RTSP_PORT "8080"

class VlcArgs
{

public:

    explicit VlcArgs(QString hostAddr, QString rtspAddress,
                                       QString port, QString sdpName,
                                                     QString networkSettings);

    //TODO: Write constructor for local file arguments
    VlcArgs();

    ~VlcArgs();

    /* Returns QStringList of arguments for player */
    QStringList list();

private:

    QStringList _argsList;
    QString     _hostAddr;
    QString     _rtspAddress;
    QString     _udpAddress;
    QString     _port;
    QString     _sdpName;
    QString     _networkSettings;

};
#endif // ARGS_H
