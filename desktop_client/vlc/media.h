#ifndef MEDIA_H
#define MEDIA_H

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QUrl>

#include "vlc/vlcinstance.h"
#include "vlc/vlc.h"
#include "vlc/state.h"

class VlcMedia : public QObject
{
    Q_OBJECT

public:

    /* Constructor for new media instance from local
     * @param   location    QString (const) -> address of media
     * @param   localFile   bool    -> in application directory or url
     * @param   instace     VlcInstance -> Instance of vlc player
     */
    explicit VlcMedia(QString &currentLocation,
                      bool local,
                      VlcInstance *instance);

    /* Destructor */
    ~VlcMedia();

    /* return libvlc struct */
    libvlc_media_t *libMedia_t() const;

    /* Get current media state */
    Vlc::State state() const;

    void stateChanged(Vlc::State &state);

private:

    libvlc_media_t *_libvlcMedia;

    QString _currentLocation;

};

#endif // MEDIA_H
