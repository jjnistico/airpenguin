#include <QtCore/QDebug>
#include <QtCore/QStringList>

#include "vlc/vlc.h"

#include "vlc/vlcinstance.h"

VlcInstance::VlcInstance(const QStringList &args, QObject *parent) :
                            QObject(parent)
{

/* Need to convert QStringList to const char * for passing to new instance
 * as well as getting the number of arguments to pass
 */
    char *argv[args.count()];
          for (int i = 0; i < args.count(); ++i)
              argv[i] = (char *)qstrdup(args.at(i).toUtf8().data());

    /* Create new libvlc instance */
    _libvlcInstance = libvlc_new(args.count(), argv);

}

/* Destructor -> release libvlc instance per API */
VlcInstance::~VlcInstance()
{
    if (_libvlcInstance)
        libvlc_release(_libvlcInstance);
}

/* Returns libvlc instance */
libvlc_instance_t *VlcInstance::libInst_t()
{
    return _libvlcInstance;
}

//
