#ifndef AUDIO_H
#define AUDIO_H

#include <QtCore/QObject>
#include <QtCore/QStringList>
#include <QtCore/QMap>

#include "vlc/mediaplayer.h"
#include "vlc/vlc.h"
#include "state.h"

class VlcMediaPlayer;

class VlcAudio : public QObject
{
    Q_OBJECT

private:

    libvlc_media_player_t *_libvlcMediaPlayer;

public:
    /* VlcAudio constructor
     * New audio manager for media playback
     * @param   player VlcMediaPlayer -> instance of player
     */
    explicit VlcAudio(VlcMediaPlayer *player);

    /* VlcAudio destructor */
    ~VlcAudio();

    /* Get current volume */
    int getVolume() const;

    /* Get mute state */
    bool getMute() const;

public slots:

    /* Set audio volume */
    void setVolume(int volume);

    /* Toggle mute of player */
    void toggleMute() const;

signals:
    /* Signal when volume changes
     * @param   volume int
     */
    void volumeChangedInt(int volume);

    /* Signal when volume changes (float)
     * @param   volume float
     */
    void volumeChangedFloat(float volume);

    /* Signal when mute status has changed
     * @param   mute bool
     */
    void muteChanged(bool mute);

};

#endif // AUDIO_H
