#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H

#include <list>
#include <string>
#include <QUdpSocket>

typedef struct  s_packet {
    uint16_t    port;
    char        name[20];
}               t_packet;

typedef struct s_AirPenguin {
    std::string name;
    std::string ip;
    int         port;
}               t_AirPenguin;

class NetworkManager
{
public:
    virtual                         ~NetworkManager() {}

    const std::list<t_AirPenguin>&  getAirPenguinList() const;
    bool                            fillAirPenguinList();
    t_AirPenguin const              getDeviceFromName(std::string) const;
    void                            connectToAP(std::string);

    /* Singleton */
    static NetworkManager&          getInstance() {
        static NetworkManager       networkManager;

        return networkManager;
    }

private:
    NetworkManager();

    std::list<t_AirPenguin>     _airPenguins;
    QUdpSocket                  _udpSocket;
};

#endif // NETWORKMANAGER_H
