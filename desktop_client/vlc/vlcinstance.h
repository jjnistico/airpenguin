#ifndef VLCINSTANCE_H
#define VLCINSTANCE_H

#include <QtCore/QObject>

#include "vlc/vlc.h"
#include "vlc/state.h"

class VlcInstance : public QObject
{

    Q_OBJECT

public:

    /* VlcInstance constructor
     * @param   QString args -> list of arguments for instantiation
     * @param   QObject parent -> Instance's parent object
     */
    explicit VlcInstance(const QStringList &args, QObject *parent = 0);

    /* VlcInstance destructor */
    ~VlcInstance();

    /* Returns libvlc instance object */
    libvlc_instance_t *libInst_t();

private:

    libvlc_instance_t *_libvlcInstance;
};

#endif // VLCINSTANCE_H
