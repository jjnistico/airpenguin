#include "vlc/args.h"
#include <QStringList>

/* Command line arguments to pass to vlc instace
 * * For a list of possible arguments see
 * https://wiki.videolan.org/VLC_command-line_help/
 */

/* Constructor for cast args object */
VlcArgs::VlcArgs(QString hostAddr, QString rtspAddress,
                 QString port, QString sdpName, QString networkSettings) :
                   _hostAddr(hostAddr),
                   _rtspAddress(rtspAddress),
                   _port(port),
                   _sdpName(sdpName),
                   _networkSettings(networkSettings)
{
    _argsList << "-vvv"
              << "--sout=#transcode{vcode=h264,vb=2048,acodec=mpga,ab=192,channels=2}:rtp{dst=" + _hostAddr + ",port=" + _port + ",sdp=rtsp"
                    "://" + _rtspAddress + ":" + RTSP_PORT + "/" + _sdpName + "}"
              << _networkSettings;
}

/* TODO: Options for local media playback */
VlcArgs::VlcArgs()
{
    _argsList << "-vv";
}

VlcArgs::~VlcArgs() {}

/* Return QStringList of args */
QStringList VlcArgs::list()
{
    return _argsList;
}
