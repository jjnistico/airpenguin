#ifndef CASTPLAYER_H
#define CASTPLAYER_H

#include <QWidget>
#include <QDockWidget>
#include <QMainWindow>
#include <QPainter>

#include "vlc/args.h"
#include "vlc/mediaplayer.h"
#include "qtinterface/volumeslider.h"
#include "qtinterface/scrubber.h"
#include "qtinterface/playbutton.h"
#include "qtinterface/stopbutton.h"

class QWidget;
class VlcArgs;
class VlcInstance;
class VlcMedia;
class VlcMediaPlayer;

class CastPlayer : public QDockWidget
{
    Q_OBJECT

public:

    explicit CastPlayer(QString filePath, VlcArgs *args, bool autoPlay, QWidget *parent = 0);

    ~CastPlayer();

signals:

    void playerClosed();

private:

    void initQtPlayer(VlcArgs *args);

    void paintEvent(QPaintEvent *event);

    VlcInstance         *_instance;
    VlcMedia            *_media;
    VlcMediaPlayer      *_player;
    QString              _filePath;
    QWidget             *_layout;
    //QtPositionScrubber  *_posScrubber;
    QtPlayButton        *_playButton;
    QtStopButton        *_stopButton;

    QtVideoFrame        *_videoFrame;

    bool                 _autoPlay;
};


#endif // CASTPLAYER_H
