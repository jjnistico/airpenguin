#ifndef PLAYER_H
#define PLAYER_H

#include <QWidget>
#include <QMainWindow>
#include <QMouseEvent>

#include "vlc/args.h"
#include "vlc/mediaplayer.h"
#include "qtinterface/volumeslider.h"
#include "qtinterface/scrubber.h"
#include "qtinterface/playbutton.h"
#include "qtinterface/stopbutton.h"
#include "qtinterface/mutebutton.h"
#include "qtinterface/fullscreenbutton.h"
#include "qtinterface/videoframe.h"

class QWidget;
class VlcInstance;
class VlcMedia;
class VlcMediaPlayer;

class LocalPlayer : public QWidget
{
    Q_OBJECT

public:

    explicit LocalPlayer(QString &filePath, VlcArgs *args, bool autoPlay, bool stream, QWidget *parent = 0);

    ~LocalPlayer();

signals:

    void playerClosed();

public slots:

   void toggleFullscreen();

private:

    void initQtPlayer();

    VlcInstance         *_instance;
    VlcMedia            *_media;
    VlcMediaPlayer      *_player;
    QString              _filePath;

    VlcArgs             *_args;
    QtVolumeSlider      *_volSlider;
    QtPositionScrubber  *_posScrubber;
    QtPlayButton        *_playButton;
    QtStopButton        *_stopButton;
    QtMuteButton        *_muteButton;
    QtFullscreenButton  *_fsButton;

    QtVideoFrame        *_videoFrame;

    bool                 _autoPlay;
    bool                 _stream;

    };

#endif // PLAYER_H
