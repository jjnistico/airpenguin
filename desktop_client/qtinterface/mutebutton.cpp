#include <QIcon>
#include <QStyle>

#include "qtinterface/mutebutton.h"

QtMuteButton::QtMuteButton(VlcMediaPlayer *player, QWidget *parent)
                           : QPushButton(parent),
                             _vlcMediaPlayer(player)
{
    initQtMuteButton();
}

QtMuteButton::~QtMuteButton()
{

}

void QtMuteButton::initQtMuteButton()
{
    QSize buttonSize(20, 20);
    this->setFixedSize(buttonSize);
    this->setIcon(QIcon(":/mutebuttonOnLight.png"));

    _vlcAudio = _vlcMediaPlayer->audio();

    _timer = new QTimer();
    _timer->start(100);
    connect(_timer, SIGNAL(timeout()), this, SLOT(updateIcon()));

    connect(this, SIGNAL(clicked()), this, SLOT(muteMedia()));
}

void QtMuteButton::muteMedia()
{
    if (!_vlcMediaPlayer->currentMedia())
    {
        this->setIcon(QIcon(":/mutebuttonOnLight.png"));
        return;
    }

    if (_vlcAudio->getMute())
        _vlcAudio->toggleMute();
    else
        _vlcAudio->toggleMute();

}

void QtMuteButton::updateIcon()
{
    if (_vlcAudio->getMute() || _vlcAudio->getVolume() == 0)
        this->setIcon(QIcon(":/mutebuttonOnLight.png"));
    else
        this->setIcon(QIcon(":/mutebuttonOffLight.png"));
}
