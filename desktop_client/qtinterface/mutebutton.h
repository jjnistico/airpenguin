#ifndef MUTEBUTTON_H
#define MUTEBUTTON_H

#include <QPushButton>
#include <QTimer>

#include "vlc/mediaplayer.h"
#include "vlc/audio.h"

class VlcMediaPlayer;

class QtMuteButton : public QPushButton
{
    Q_OBJECT

public:

    explicit QtMuteButton(VlcMediaPlayer *player, QWidget *parent = 0);

    ~QtMuteButton();

private slots:

    void muteMedia();

    void updateIcon();

private:

    void initQtMuteButton();

    VlcMediaPlayer *_vlcMediaPlayer;

    VlcAudio       *_vlcAudio;

    QTimer         *_timer;

};

#endif // MUTEBUTTON_H
