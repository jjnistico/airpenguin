#include <QPushButton>
#include <QIcon>
#include <QStyle>

#include "qtinterface/fullscreenbutton.h"

QtFullscreenButton::QtFullscreenButton(QWidget *parent)
                                      : QPushButton(parent)
{
    initQtFSButton();
}

QtFullscreenButton::~QtFullscreenButton()
{

}

void QtFullscreenButton::initQtFSButton()
{
    QSize buttonSize(20,20);
    this->setFixedSize(buttonSize);
    this->setIcon(QIcon(":/fsbuttonLight.png"));

    connect(this, SIGNAL(clicked(bool)), this, SLOT(toggleFullscreen()));
}

void QtFullscreenButton::toggleFullscreen()
{
    emit emitFullscreen();
}
