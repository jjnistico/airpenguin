#include "qtinterface/videoframe.h"

/* Subclass to implement mouse events (double click for fullscreen)
 * in videoWidget
 */
QtVideoFrame::QtVideoFrame(QWidget *parent) : QWidget(parent)
{

}

QtVideoFrame::~QtVideoFrame() {}

void QtVideoFrame::mouseDoubleClickEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        emit doubleClicked();
    }
}
