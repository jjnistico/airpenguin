#include <QPushButton>
#include <QIcon>
#include <QStyle>

#include "qtinterface/stopbutton.h"

QtStopButton::QtStopButton(VlcMediaPlayer *player, QWidget *parent)
                           : QPushButton(parent),
                             _vlcMediaPlayer(player)
{
    initQtStopButton();
}

QtStopButton::QtStopButton(QWidget *parent)
                           : QPushButton(parent),
                             _vlcMediaPlayer(0)
{
    initQtStopButton();
}

QtStopButton::~QtStopButton()
{

}

void QtStopButton::initQtStopButton()
{
    QSize buttonSize(40,40);
    this->setFixedSize(buttonSize);
    this->setIcon(QIcon(":/stopbuttonLight.png"));

    connect(this, SIGNAL(clicked(bool)), this, SLOT(stopMedia()));
}

void QtStopButton::stopMedia()
{
    if (!_vlcMediaPlayer)
        return;

    if (_vlcMediaPlayer->state() == Vlc::Playing
     || _vlcMediaPlayer->state() == Vlc::Buffering
     || _vlcMediaPlayer->state() == Vlc::Paused)
        _vlcMediaPlayer->stop();
}
