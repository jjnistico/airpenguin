#ifndef STOPBUTTON_H
#define STOPBUTTON_H

#include <QPushButton>

#include "vlc/mediaplayer.h"

class VlcMediaPlayer;

class QtStopButton : public QPushButton
{
    Q_OBJECT

public:

    explicit QtStopButton(VlcMediaPlayer *player, QWidget *parent = 0);

    explicit QtStopButton(QWidget *parent = 0);

    ~QtStopButton();

private slots:

    void stopMedia();

private:

    void initQtStopButton();

    VlcMediaPlayer *_vlcMediaPlayer;
};

#endif // STOPBUTTON_H
