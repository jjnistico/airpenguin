#ifndef VIDEOFRAME_H
#define VIDEOFRAME_H

#include <QWidget>
#include <QMouseEvent>

class QtVideoFrame : public QWidget
{
    Q_OBJECT

public:

    QtVideoFrame(QWidget *parent);

    ~QtVideoFrame();

protected:

    void mouseDoubleClickEvent(QMouseEvent *event);

signals:

    void doubleClicked();
};

#endif // VIDEOFRAME_H
