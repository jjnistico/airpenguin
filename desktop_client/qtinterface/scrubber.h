
#ifndef SCRUBBER_H
#define SCRUBBER_H

#include <QSlider>
#include <QTimer>
#include "vlc/mediaplayer.h"

class QTimer;

class VlcMediaPlayer;

class QtPositionScrubber : public QSlider
{
    Q_OBJECT

public:

    /* PositionScrubber constructor
     * @param   player  VlcMediaPlayer -> player object to connect to
     * @param   parent  QWidget -> pointer to parent QWidget
     */
    explicit QtPositionScrubber(VlcMediaPlayer *player, QWidget *parent = 0);

    ~QtPositionScrubber();

public slots:

    /* Returns playback position */
    float playbackPosition() const;

    /* Sets scrubber position */
    void setScrubberPosition(int position);

private slots:
    /* Update playback position of player */
    void updatePlaybackPosition();

    /* Update position of scrubber graphic in player */
    void updateScrubberDisplay();

    /* Click along scrubber bar to change position */
    void clickToPosition();

private:

    void initQtPositionScrubber();

    /* Function to convert float pos->int pos
     * @param   pos float -> position of media playback
     */
    float slideToPlayback(int slidePos);
    int playToSlide();

    VlcMediaPlayer *_vlcMediaPlayer;

    int   _qtSlidePos;
    float _currentPlayPos;

    QTimer  *_timer;
};

#endif // SCRUBBER_H
