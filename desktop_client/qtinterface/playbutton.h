#ifndef PLAYBUTTON_H
#define PLAYBUTTON_H

#include <QPushButton>
#include "vlc/mediaplayer.h"

class VlcMediaPlayer;

class QtPlayButton : public QPushButton
{
    Q_OBJECT

public:

    explicit QtPlayButton(VlcMediaPlayer *player, QWidget *parent = 0);

    explicit QtPlayButton(QWidget *parent = 0);

    ~QtPlayButton();

private slots:

    void playMedia();

    void updateIcon();

private:

    void initQtPlayButton();

    VlcMediaPlayer *_vlcMediaPlayer;

    QTimer         *_timer;
};

#endif // PLAYBUTTON_H
