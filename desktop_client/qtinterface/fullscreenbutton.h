#ifndef FULLSCREENBUTTON_H
#define FULLSCREENBUTTON_H

#include <QPushButton>
#include "vlc/mediaplayer.h"

class VlcMediaplayer;

class QtFullscreenButton : public QPushButton
{
    Q_OBJECT

public:

    explicit QtFullscreenButton(QWidget *parent = 0);

    ~QtFullscreenButton();

private slots:

    void toggleFullscreen();

signals:

    void emitFullscreen();

private:

    void initQtFSButton();
};

#endif // FULLSCREENBUTTON_H
