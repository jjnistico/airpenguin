#ifndef ERROR_H
#define ERROR_H

#include <QMessageBox>
#include <QPainter>
#include <QTimer>

class QtError : QMessageBox
{
    Q_OBJECT

public:

    explicit QtError(QString errMsg, QWidget *parent=0);

    ~QtError();

private:

    void initQtError();

    void hide();

    void paintEvent(QPaintEvent *event);

    QString         _errMsg;

};

#endif // ERROR_H
