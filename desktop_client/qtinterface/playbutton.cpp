#include <QPushButton>
#include <QIcon>
#include <QStyle>

#include "qtinterface/playbutton.h"

QtPlayButton::QtPlayButton(VlcMediaPlayer *player, QWidget *parent)
                           : QPushButton(parent),
                             _vlcMediaPlayer(player)
{
    initQtPlayButton();
}

QtPlayButton::QtPlayButton(QWidget *parent)
                          : QPushButton(parent),
                            _vlcMediaPlayer(0)
{
    initQtPlayButton();
}

QtPlayButton::~QtPlayButton()
{

}

void QtPlayButton::initQtPlayButton()
{
    QSize buttonSize(40,40);
    this->setFixedSize(buttonSize);
    this->setIcon(QIcon(":/playbuttonLight.png"));

    //timer for button updates
    _timer = new QTimer();
    _timer->start(100);
    connect(_timer, SIGNAL(timeout()), this, SLOT(updateIcon()));

    connect(this, SIGNAL(clicked(bool)), this, SLOT(playMedia()));
}

void QtPlayButton::playMedia()
{
    if (!_vlcMediaPlayer->currentMedia())
    {
        this->setIcon(QIcon(":/playbuttonLight.png"));
        return;
    }

    if (_vlcMediaPlayer->state() == Vlc::Paused
    || _vlcMediaPlayer->state() == Vlc::Idle
    || _vlcMediaPlayer->state() == Vlc::Stopped)
        _vlcMediaPlayer->play();
    else
        _vlcMediaPlayer->pause();

}

void QtPlayButton::updateIcon()
{
    if (_vlcMediaPlayer->state() == Vlc::Paused
     || _vlcMediaPlayer->state() == Vlc::Idle
     || _vlcMediaPlayer->state() == Vlc::Stopped)
        this->setIcon(QIcon(":/playbuttonLight.png"));
    else
        this->setIcon(QIcon(":/pausebuttonLight.png"));
}
